# Week 1 Mini Project
Author: Oliver Chen (yc557)

## Screenshots

![Homepage](./readme-images/homepage.png)
![About](./readme-images/about.png)

## Correct Zola usage
This porfolio pages used [Zola](https://www.getzola.org/documentation/getting-started/overview/) and one of its [themes](https://www.getzola.org/themes/).

## Page templates
For each project in the potfolio, we can use the page templates to fill out the information we want to showcase. Here is an [example](https://yushunchen.gitlab.io/portfolio/overview-abridge/).

## Styling
This project is styled with CSS/SCSS. For example, I have customized styles in files such as `abdrige.scss` and `_extra.scss`.

## Links
- GitHub repo link: https://gitlab.com/YushunChen/portfolio

- Live static site: https://yushunchen.gitlab.io/portfolio/