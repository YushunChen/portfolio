+++
title = "About"
path = "about"
template = "pages.html"
draft = false
+++

# About Me

## Introduction

Hello! My name is Yushun (Oliver) Chen. I am currently pursuing a Master's degree in Electrical and Computer Engineering, specializing in software development, at Duke University. I completed my undergraduate studies at the University of Wisconsin-Madison in May 2022, where I triple-majored in Computer Sciences, Statistics, and Applied Mathematics.

## Education

### Duke University (Sep 2022 - May 2024, estimated)

- **Degree:** M.S. Electrical and Computer Engineering
- **GPA:** NA/4.000

Life has come full circle as I fell in love with Duke during my high school days in Charlotte, NC. Returning to a familiar state and becoming a student at Duke is a rewarding experience. The rigorous academic standards and the beautiful campus are what attracted me the most. I took the picture to the right during my visit in 2018, and I vividly remember the feeling of walking on the campus.

### University of Wisconsin-Madison (Sep 2019 - May 2022)

- **Degrees:** 
  - B.S. Computer Sciences
  - B.S. Statistics
  - B.S. Applied Mathematics
- **GPA:** 3.938/4.000

UW-Madison had everything I desired in a university: high academic standards, a vibrant sports culture, scenic beauty, and the presence of favorite people. Beyond academics, I was actively involved on campus. I worked at Liz's Market, a dining hall, which marked my first job in the United States. Additionally, I served as a note-taker for the McBurney Disability Resource Center.

## Links

- [Resume](link_to_your_resume)
- [Course Guide](link_to_your_course_guide)

## Future Research Interests

Looking ahead, my interests lie in the fields of software engineering and Human-Computer Interaction (HCI). I am excited about the opportunities for research and exploration in these areas.

Feel free to reach out if you have any questions or if you would like to connect. I look forward to the journey ahead!
